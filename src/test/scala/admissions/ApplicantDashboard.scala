package admissions

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class ApplicantDashboard extends Simulation {

	val url = Option(System.getProperty("url"))
	val concurrencyValue = Option(System.getProperty("concurrency"))
	val maxConcurrencyValue = Option(System.getProperty("max_concurrency"))
	val disableProxy = Option(System.getProperty("no_proxy"))
	val constantDurationValue = Option(System.getProperty("constant_duration"))
	val rampDurationValue = Option(System.getProperty("ramp_duration"))

	val defaultUrl = "https://preprod.sls.bristol.ac.uk"

	val loadUrl = url.map(_.toString).getOrElse(defaultUrl)
	val concurrency = concurrencyValue.map(_.toInt).getOrElse(1)
	val maxConcurrency = maxConcurrencyValue.map(_.toInt).getOrElse(1)
	val constantDuration = constantDurationValue.map(_.toInt).getOrElse(1).minutes
	val rampDuration = rampDurationValue.map(_.toInt).getOrElse(1).minutes

	val feeder = csv("admissions/applicant_credentials_SLS_Pre_Prod.csv").circular

	//val ssoLink = "${Link}"

	val httpProtocol = http
		.baseUrl(loadUrl)
		.inferHtmlResources(WhiteList(), BlackList())
		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-US,en;q=0.9")
		.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36")

	val headers_0 = Map(
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "none",
		"Sec-Fetch-User" -> "?1",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_1 = Map(
		"Accept" -> "text/html, */*; q=0.01",
		"Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
		"Origin" -> defaultUrl,
		"Sec-Fetch-Dest" -> "empty",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_2 = Map(
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "same-origin",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_3 = Map(
		"Accept" -> "*/*",
		"Sec-Fetch-Dest" -> "script",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_5 = Map(
		"Accept" -> "text/css,*/*;q=0.1",
		"Sec-Fetch-Dest" -> "style",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_11 = Map(
		"Origin" -> defaultUrl,
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "same-origin",
		"Sec-Fetch-User" -> "?1",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_12 = Map(
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "same-origin",
		"Sec-Fetch-User" -> "?1",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_17 = Map(
		"Accept" -> "image/webp,image/apng,image/*,*/*;q=0.8",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")


	val scn = scenario("ApplicantDashboardNavigation")
		.feed(feeder)
		.exec(http("Dashboard Pre-Redirect")
			.get("/urd/sits.urd/run/siw_sso.go?${Link}")
			.headers(headers_0)
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0000_response.html")))
			.check(regex("""value="[^?]*\?([^"]*)""").saveAs("startUrl"))
      .check(regex("""rednkey = "(.+?)"""").saveAs("nKey"))
      .check(status.is(200))
      .check(substring("You have successfully")))
		.pause(1)

		.exec(http("Set_Screensize")
			.post("/urd/sits.urd/run/SIW_LGN_RED.set_screen_dimension")
			.headers(headers_1)
			.formParam("NKEY", "${nKey}")
			.formParam("HEIGHT", "800")
			.formParam("WIDTH", "1280")
			.resources(http("Post_Redirect_Login")
			.get("/urd/sits.urd/run/SIW_YMHD.start_url?" + "${startUrl}")
			.headers(headers_2)
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0002_response.html")))
      .check(regex("""name="NKEY.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("loginNkey"))
      .check(regex("""name="MHDC.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("mhdc"))
      .check(regex("""sits_user_timeout = "([^"]*)""").saveAs("userTimeout"))
      .check(regex("""name="%.WEB_HEAD.MENSYS.1" value="([^"]*)""").saveAs("rdWebHead"))
      .check(regex("""name="RUN_MODE.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("rdRunMode"))
      .check(regex("""name="SEQN.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("rdSeqn"))
      .check(regex("""name="%.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("rdDummyMensys"))
      .check(regex("""name="#.TTE.MENSYS.1-1" value="([^"]*)""").saveAs("rdTteMensys"))
      .check(regex("""name="#.TTQ.MENSYS.1" value="([^"]*)""").saveAs("rdTtqMensys1"))
      .check(regex("""name="#.TTQ.MENSYS.2" value="([^"]*)""").saveAs("rdTtqMensys2"))
      .check(regex("""name="#.TTQ.MENSYS.3" value="([^"]*)""").saveAs("rdTtqMensys3"))
      .check(regex("""name="#.TTQ.MENSYS.4" value="([^"]*)""").saveAs("rdTtqMensys4")))
      .check(status.is(200)))
		.pause(6)

		.exec(http("StudentNo_Validation")
			.post("/urd/sits.urd/run/siw_ttq.validation")
			.headers(headers_1)
			.formParam("mode", "ONE")
			.formParam("indx", "ANSWER.TTQ.MENSYS.2.")
			.formParam("valu", "${Student}")
			.formParam("ttqs", "1")
			.formParam("mhdc", "${mhdc}")
			.formParam("seqn", "1")
			.formParam("nkey", "${loginNkey}")
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0007_response.html")))
      .check(status.in(200, 302)))
		.pause(6)

		.exec(http("Surname_Validation")
			.post("/urd/sits.urd/run/siw_ttq.validation")
			.headers(headers_1)
			.formParam("mode", "ONE")
			.formParam("indx", "ANSWER.TTQ.MENSYS.3.")
			.formParam("valu", "${Surname}")
			.formParam("ttqs", "2")
			.formParam("mhdc", "${mhdc}")
			.formParam("seqn", "1")
			.formParam("nkey", "${loginNkey}")
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0008_response.html")))
      .check(status.is(200)))
		.pause(5)

		.exec(http("DOB_Validation")
			.post("/urd/sits.urd/run/siw_ttq.validation")
			.headers(headers_1)
			.formParam("mode", "ONE")
			.formParam("indx", "ANSWER.TTQ.MENSYS.4.")
			.formParam("valu", "${DOB}")
			.formParam("ttqs", "3")
      .formParam("mhdc", "${mhdc}")
			.formParam("seqn", "1")
      .formParam("nkey", "${loginNkey}")
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0010_response.html")))
      .check(status.is(200)))

    .exec(http("Complete_Login")
			.post("/urd/sits.urd/run/SIW_TTQ")
			.headers(headers_11)
			.formParam("%.WEB_HEAD.MENSYS.1", "${rdWebHead}")
			.formParam("MHDC.DUMMY.MENSYS.1", "${mhdc}")
			.formParam("SEQN.DUMMY.MENSYS.1", "${rdSeqn}")
			.formParam("NKEY.DUMMY.MENSYS.1", "${loginNkey}")
			.formParam("RUN_MODE.DUMMY.MENSYS.1", "${rdRunMode}")
			.formParam("%.DUMMY.MENSYS.1", "${rdDummyMensys}")
			.formParam("#.TTE.MENSYS.1-1", "${rdTteMensys}")
			.formParam("DUM_FIXT.TTQ.MENSYS.1", "")
			.formParam("ANSWER.TTQ.MENSYS.2", "${Student}")
			.formParam("DUM_FIXT.TTQ.MENSYS.2", "")
			.formParam("ANSWER.TTQ.MENSYS.3", "${Surname}")
			.formParam("DUM_FIXT.TTQ.MENSYS.3", "")
			.formParam("ANSWER.TTQ.MENSYS.4", "${DOB}")
			.formParam("DUM_FIXT.TTQ.MENSYS.4", "")
			.formParam("#.TTQ.MENSYS.1", "${rdTtqMensys1}")
			.formParam("#.TTQ.MENSYS.2", "${rdTtqMensys2}")
			.formParam("#.TTQ.MENSYS.3", "${rdTtqMensys3}")
			.formParam("#.TTQ.MENSYS.4", "${rdTtqMensys4}")
			.formParam("NEXT.DUMMY.MENSYS.1", "Confirm")
			.formParam("#.TTE.MENSYS.1-1", "${rdTteMensys}")
			.formParam("%.DUMMY.MENSYS.1", "${rdDummyMensys}")
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0011_response.html")))
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0009_response.html")))
		/*	.check(
			checkIf((response: Response, session: Session) => response.status.equals(substring("University of Bristol UCAS Application dashboard"))) {
				regex("""SIW_PORTAL.URL\?([^"]+)""").saveAs("portalUrl")
				}
			)*/
			.check(substring("University of Bristol UCAS"))
			.check(regex("""SIW_PORTAL.URL\?([^"]+)""").saveAs("portalUrl"))
			.check(status.is(200)))
    .pause(2)

    .exec(http("Dashboard_Homepage")
			.get("/urd/sits.urd/run/SIW_PORTAL.URL?" + "${portalUrl}")
			.headers(headers_12)
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0012_response.html")))
      .check(regex("""SIW_YMHD.start_url\?([^"]+)"""").find(0).saveAs("uploadDocs"))
      .check(regex("""SIW_YMHD.start_url\?([^"]+)"""").find(1).saveAs("changeApp"))
      .check(status.is(200))
      .check(substring("Applicant Dashboard")))
		.pause(4)

    .exec(http("Upload_Docs")
			.get("/urd/sits.urd/run/SIW_YMHD.start_url?" + "${uploadDocs}")
			.headers(headers_12)
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0013_response.html")))
      .check(regex("""name="MHDC.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("uploadDocsMhdc"))
      .check(regex("""name="NKEY.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("uploadDocsNkey"))
      .check(regex("""name="%.WEB_HEAD.MENSYS.1" value="([^"]*)""").saveAs("uploadDocsWebHead"))
      .check(regex("""name="RUN_MODE.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("uploadDocsRunMode"))
      .check(regex("""name="%.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("uploadDocsDummyMensys"))
      .check(regex("""name="#.TTE.MENSYS.1-1" value="([^"]*)""").saveAs("uploadDocsTteMensys"))
      .check(regex("""name="#.TTQ.MENSYS.1" value="([^"]*)""").saveAs("uploadDocsTtqMensys1"))
      .check(regex("""name="#.TTQ.MENSYS.2" value="([^"]*)""").saveAs("uploadDocsTtqMensys2"))
      .check(status.is(200))
      .check(substring("Please select the document type")))
		.pause(3)

    .exec(http("UploadDocs_Validation")
			.post("/urd/sits.urd/run/siw_ttq.validation")
			.headers(headers_1)
			.formParam("mode", "ONE")
			.formParam("indx", "ANSWER.TTQ.MENSYS.1.")
			.formParam("valu", "ADM014")
			.formParam("ttqs", "1")
			.formParam("mhdc", "${uploadDocsMhdc}")
			.formParam("seqn", "2")
			.formParam("nkey", "${uploadDocsNkey}")
			.resources(http("ApplicantDashboard_15")
			.post("/urd/sits.urd/run/SIW_TTQ")
			.headers(headers_11)
			.formParam("%.WEB_HEAD.MENSYS.1", "${uploadDocsWebHead}")
      .formParam("MHDC.DUMMY.MENSYS.1", "${uploadDocsMhdc}")
      .formParam("SEQN.DUMMY.MENSYS.1", "2")
			.formParam("NKEY.DUMMY.MENSYS.1", "${uploadDocsNkey}")
			.formParam("RUN_MODE.DUMMY.MENSYS.1", "${uploadDocsRunMode}")
			.formParam("%.DUMMY.MENSYS.1", "${uploadDocsDummyMensys}")
			.formParam("#.TTE.MENSYS.1-1", "${uploadDocsTteMensys}")
			.formParam("ANSWER.TTQ.MENSYS.1", "ADM014")
			.formParam("DUM_FIXT.TTQ.MENSYS.1", "")
			.formParam("DUM_FIXT.TTQ.MENSYS.2", "")
			.formParam("#.TTQ.MENSYS.1", "${uploadDocsTtqMensys1}")
			.formParam("#.TTQ.MENSYS.2", "${uploadDocsTtqMensys2}")
			.formParam("NEXT.DUMMY.MENSYS.1", "Next")
			.formParam("#.TTE.MENSYS.1-1", "${uploadDocsTteMensys}")
			.formParam("%.DUMMY.MENSYS.1", "${uploadDocsDummyMensys}")
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0015_response.html")))
      .check(substring("Please upload your document using the upload button below.")))
      .check(status.is(200)))

    .exec(http("Dashboard_home_2")
			.get("/urd/sits.urd/run/SIW_PORTAL.URL?" + "${portalUrl}")
			.headers(headers_12)
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0020_response.html")))
      .check(status.is(200))
      .check(substring("Applicant Dashboard")))
		.pause(1)

    .exec(http("Change_Application")
			.get("/urd/sits.urd/run/SIW_YMHD.start_url?" + "${changeApp}")
			.headers(headers_12)
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0021_response.html")))
      .check(regex("""name="MHDC.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("changeAppMhdc"))
      .check(regex("""name="NKEY.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("changeAppNkey"))
      .check(regex("""name="%.WEB_HEAD.MENSYS.1" value="([^"]*)""").saveAs("changeAppWebHead"))
      .check(regex("""name="RUN_MODE.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("changeAppRunMode"))
      .check(regex("""name="%.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("changeAppDummyMensys"))
      .check(regex("""name="#.TTE.MENSYS.1-1" value="([^"]*)""").saveAs("changeAppTteMensys"))
      .check(regex("""name="#.TTQ.MENSYS.1" value="([^"]*)""").saveAs("changeAppTtqMensys1"))
      .check(regex("""name="#.TTQ.MENSYS.2" value="([^"]*)""").saveAs("changeAppTtqMensys2"))
      .check(status.is(200))
      .check(substring("Request a change to your application")))
		.pause(2)

    .exec(http("Change_Application_Validation")
			.post("/urd/sits.urd/run/siw_ttq.validation")
			.headers(headers_1)
			.formParam("mode", "ONE")
			.formParam("indx", "ANSWER.TTQ.MENSYS.1.1")
			.formParam("valu", "1")
			.formParam("ttqs", "1")
			.formParam("mhdc", "${changeAppMhdc}")
			.formParam("seqn", "2")
			.formParam("nkey", "${changeAppNkey}")
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0022_response.html")))
      .check(status.is(200)))
		.pause(2)

    .exec(http("Course_Change")
			.post("/urd/sits.urd/run/SIW_TTQ")
			.headers(headers_11)
			.formParam("%.WEB_HEAD.MENSYS.1", "${changeAppWebHead}")
			.formParam("MHDC.DUMMY.MENSYS.1", "${changeAppMhdc}")
			.formParam("SEQN.DUMMY.MENSYS.1", "2")
			.formParam("NKEY.DUMMY.MENSYS.1", "${changeAppNkey}")
			.formParam("RUN_MODE.DUMMY.MENSYS.1", "${changeAppRunMode}")
			.formParam("%.DUMMY.MENSYS.1", "${changeAppDummyMensys}")
			.formParam("#.TTE.MENSYS.1-1", "${changeAppTteMensys}")
			.formParam("ANSWER.TTQ.MENSYS.1", "1")
			.formParam("DUM_FIXT.TTQ.MENSYS.1", "")
			.formParam("DUM_FIXT.TTQ.MENSYS.2", "")
			.formParam("#.TTQ.MENSYS.1", "${changeAppTtqMensys1}")
			.formParam("#.TTQ.MENSYS.2", "${changeAppTtqMensys2}")
			.formParam("NEXT.DUMMY.MENSYS.1", "Next")
			.formParam("#.TTE.MENSYS.1-1", "${changeAppTteMensys}")
			.formParam("%.DUMMY.MENSYS.1", "${changeAppDummyMensys}")
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0023_response.html")))
      .check(regex("""name="MHDC.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("courseChangeMhdc"))
      .check(regex("""name="NKEY.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("courseChangeNkey"))
      .check(regex("""name="%.WEB_HEAD.MENSYS.1" value="([^"]*)""").saveAs("courseChangeWebHead"))
      .check(regex("""name="RUN_MODE.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("courseChangeRunMode"))
      .check(regex("""name="%.DUMMY.MENSYS.1" value="([^"]*)""").saveAs("courseChangeDummyMensys"))
      .check(regex("""name="#.TTE.MENSYS.1-1" value="([^"]*)""").saveAs("courseChangeTteMensys"))
      .check(regex("""name="#.TTQ.MENSYS.1" value="([^"]*)""").saveAs("courseChangeTtqMensys1"))
      .check(regex("""name="#.TTQ.MENSYS.2" value="([^"]*)""").saveAs("courseChangeTtqMensys2"))
			.check(regex("""name="#.TTQ.MENSYS.3" value="([^"]*)""").saveAs("courseChangeTtqMensys3"))
			.check(regex("""name="#.TTQ.MENSYS.4" value="([^"]*)""").saveAs("courseChangeTtqMensys4"))
			.check(status.is(200))
      .check(substring("Request a course change")))
		.pause(5)

    .exec(http("ApplicantDashboard_24")
			.post("/urd/sits.urd/run/SIW_TTQ")
			.headers(headers_11)
			.formParam("%.WEB_HEAD.MENSYS.1", "${courseChangeWebHead}")
			.formParam("MHDC.DUMMY.MENSYS.1", "${courseChangeMhdc}")
			.formParam("SEQN.DUMMY.MENSYS.1", "3")
			.formParam("NKEY.DUMMY.MENSYS.1", "${courseChangeNkey}")
			.formParam("RUN_MODE.DUMMY.MENSYS.1", "${courseChangeRunMode}")
			.formParam("%.DUMMY.MENSYS.1", "${courseChangeDummyMensys}")
			.formParam("#.TTE.MENSYS.1-1", "${courseChangeTteMensys}")
			.formParam("DUM_FIXT.TTQ.MENSYS.1", "")
			.formParam("DUM_FIXT.TTQ.MENSYS.2", "")
			.formParam("ANSWER.TTQ.MENSYS.3", "")
			.formParam("DUM_FIXT.TTQ.MENSYS.3", "")
			.formParam("ANSWER.TTQ.MENSYS.4", "")
			.formParam("DUM_FIXT.TTQ.MENSYS.4", "")
			.formParam("#.TTQ.MENSYS.1", "${courseChangeTtqMensys1}")
			.formParam("#.TTQ.MENSYS.2", "${courseChangeTtqMensys2}")
			.formParam("#.TTQ.MENSYS.3", "${courseChangeTtqMensys3}")
			.formParam("#.TTQ.MENSYS.4", "${courseChangeTtqMensys4}")
			.formParam("BACK.DUMMY.MENSYS.1", "Back")
			.formParam("#.TTE.MENSYS.1-1", "${courseChangeTteMensys}")
			.formParam("%.DUMMY.MENSYS.1", "${courseChangeDummyMensys}")
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0024_response.html")))
      .check(substring("Request a change to your application"))
      .check(status.is(200)))
		.pause(1)

    .exec(http("ApplicantDashboard_25")
			.get("/urd/sits.urd/run/SIW_PORTAL.URL?" + "${portalUrl}")
			.headers(headers_12)
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0025_response.html")))
      .check(regex("""SIW_LGN_LOGOUT.start_url\?([^"]+)""").find(0).saveAs("logoutUrl"))
      .check(status.is(200))
      .check(substring("Applicant Dashboard")))
		.pause(11)

    .exec(http("ApplicantDashboard_26")
			.get("/urd/sits.urd/run/SIW_LGN_LOGOUT.start_url?" + "${logoutUrl}")
			.headers(headers_12)
			//.check(bodyBytes.is(RawFileBody("admissions/applicantdashboard/0026_response.html")))
      .check(status.is(200))
      .check(substring("You have successfully logged out of the system.")))

	setUp(scn.inject(atOnceUsers(1)).protocols(httpProtocol))

	/*setUp(scn.inject( constantUsersPerSec(concurrency) during constantDuration,
    rampUsersPerSec(concurrency) to maxConcurrency during rampDuration
  )).protocols(httpProtocol)*/

	before {
		println("Simulation is about to start!")
	}

	after {
		println("Simulation is finished!")
	}

}